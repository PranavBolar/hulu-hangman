# README #

Submission for Hulu's recruitment test by pbolar@andrew.cmu.edu

## Setup ##
```mvn package```

## To Run ##
* Main method
    *   ```com.hulu.recruitment.gallows.pbolar.hangman_solver.App```
* Program continues infinitely until manually terminated